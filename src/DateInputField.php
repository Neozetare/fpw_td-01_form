<?php

// Représente des input date
class DateInputField extends AbstractInputField {

    public function __construct($id, $name, $value = null) {
        parent::__construct('date', $id, $name);

        if (!is_null($value))
            $this->setValue($value);
    }

    // Un input date est valide s'il est valide comme champ (appel parent) et si c'est une date
    public function isValueFieldValid($value) {
        return parent::isValueFieldValid($value)
            && strtotime($value);
    }

}

?>