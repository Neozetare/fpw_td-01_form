<?php

// Classe des input checkbox
class CheckboxInputField extends AbstractInputField {

    public function __construct($id, $name, $value = null) {
        parent::__construct('checkbox', $id, $name);

        if (!is_null($value))
            $this->setValue($value);
    }

    //  Une checkbox est forcément valide, même si sa valeur est vide (valeur par défaut)
    public function isValueFieldValid($value) {
        return true;
    }

}

?>