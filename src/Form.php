<?php

// Classe "principale" qui représente un formulaire
class Form {

    const VALID_METHODS = ['GET', 'POST'];
    const DEFAULT_ACTION = '/';
    const DEFAULT_METHOD = 'POST';

    private $name;
    private $action;
    private $method;
    private $fields;

    public function __construct($name, $action, $method) {
        $this->setName($name);
        $this->setAction($action);
        $this->setMethod($method);
        // fields est un array simplement parce que les array sont des dictionnaires ordonnés, ce qui me permet d'accéder facilement aux champs et de préserver leur ordre d'arrivée
        $this->fields = array();
    }

    // Permet de décoder le JSON
    // Je ne m'étendrai pas là dessus puis que les champs peuvent être simplement rajouter avec des méthodes, j'ai rajouter ceci parce que je préférais travailler directement avec un fichier JSON pour mon contenu
    public static function fromJson($json) {
        $decoded = json_decode($json);
        if (is_null($decoded) || !is_object($decoded))
            Util::throwInvalidArgumentException('decoded', $decoded, "must be a valid JSON object with a property `name`");
        if (!array_key_exists('name', $decoded))
            Util::throwInvalidArgumentException('decoded', $decoded, "must have a `name` property");

        $action = $decoded->action ?? self::DEFAULT_ACTION;
        $method = $decoded->method ?? self::DEFAULT_METHOD;

        $form = new self($decoded->name, $action, $method);
        if (isset($decoded->fields))
            $form->addFieldsFromDecodedJson($decoded->fields);

        return $form;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        if (!is_string($name)
            || preg_match(Util::SLUG_REGEX, $name) == 0)
            Util::throwInvalidArgumentException('name', $name, 'must be a string that starts with an alpha and contains only alphanumerics, underscores and hyphens');
        $this->name = $name;
    }

    public function getAction() {
        return $this->name;
    }

    public function setAction($action) {
        if (!is_string($action))
            Util::throwInvalidArgumentException('action', $action, 'must be a string');
        $this->action = $action;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setMethod($method) {
        if (!in_array($method, Form::VALID_METHODS))
            Util::throwInvalidArgumentException('method', $method, "must be one of {Form::VALID_METHODS}");
        $this->method = $method;
    }

    // Ajoute un champs dans le formulaire
    public function addField($field) {
        if (!$field instanceof AbstractField || array_key_exists($field->getId(), $this->fields))
            Util::throwInvalidArgumentException('field', $field, "must be an AbstractField with a id different to all of the fields in the form");
        $this->fields[$field->getId()] = $field;
    }

    public function removeField($field) {
        if (!in_array($field, $this->fields))
            Util::throwInvalidArgumentException('field', $field, "must be a member of the form");
        unset($this->fields[$field->getName()]);
    }

    // Permet de décoder le JSON
    public function addFieldFromJson($jsonField) {
        $decodedField = json_decode($jsonField);
        if (is_null($decodedField))
            Util::throwInvalidArgumentException('jsonField', $jsonField, "must be a valid JSON object");
        $this->addFieldFromDecodedJson($decodedField);
    }

    // Permet de décoder le JSON
    private function addFieldFromDecodedJson($decodedField) {
        if (!is_object($decodedField))
            Util::throwInvalidArgumentException('decodedField', $decodedField, "must be a valid JSON object");

        if (!array_key_exists('type', $decodedField))
            Util::throwInvalidArgumentException('decodedField', $decodedField, "must have a `type` property with a valid field type");
        if (!array_key_exists('name', $decodedField))
            Util::throwInvalidArgumentException('decodedField', $decodedField, "must have a `name` property");

        $value = ($decodedField->value) ?? null;

        $field = null;
        switch ($decodedField->type) {
            case 'text':
                $field = new TextInputField("{$this->getName()}-{$decodedField->name}", $decodedField->name, $value);
                break;

            case 'date':
                $field = new DateInputField("{$this->getName()}-{$decodedField->name}", $decodedField->name, $value);
                break;

            case 'email':
                $field = new EmailInputField("{$this->getName()}-{$decodedField->name}", $decodedField->name, $value);
                break;

            case 'range':
                $field = new RangeInputField("{$this->getName()}-{$decodedField->name}", $decodedField->name, $value);
                break;

            case 'checkbox':
                $field = new CheckboxInputField("{$this->getName()}-{$decodedField->name}", $decodedField->name, $value);
                break;

            case 'textarea':
                $field = new TextareaField("{$this->getName()}-{$decodedField->name}", $decodedField->name, $value);
                break;

            default:
                Util::throwInvalidArgumentException('decodedField', $decodedField, "must have a `type` property with a valid field type");
                break;
        }

        foreach ($decodedField as $key => $value) {
            $method_name = 'set' . ucfirst($key);
            if (method_exists($field, $method_name))
                $field->$method_name($value);
            else {
                if (!ctype_lower($key))
                    Util::throwInvalidArgumentException('decodedField', $decodedField, "must have only lowercase keys");

                if ($value === true)
                    $field->setAdditionalAttribute($key);
                else
                    $field->setAdditionalAttribute($key, $value);
            }
        }

        $this->addField($field);
    }

    // Permet de décoder le JSON
    public function addFieldsFromJson($jsonFields) {
        $decodedFields = json_decode($jsonFields);
        if (is_null($decodedFields))
            Util::throwInvalidArgumentException('jsonFields', $jsonFields, "must be a valid JSON array");

        $this->addFieldsFromDecodedJson($decodedFields);
    }

    // Permet de décoder le JSON
    public function addFieldsFromDecodedJson($decodedFields) {
        if (!is_array($decodedFields))
            Util::throwInvalidArgumentException('jsonFields', $jsonFields, "must be a valid JSON array");

        foreach ($decodedFields as $decodedField)
            $this->addFieldFromDecodedJson($decodedField);
    }

    // Affiche le fomulaire, en affichant chaque champs
    public function display() {
        echo "<form name='{$this->name}' action='{$this->action}' method='{$this->method}'>";

        foreach ($this->fields as $field) {
            if ($field instanceof CheckboxInputField)
                echo '<div class="field-wrapper fw-inline">';
            else
                echo '<div class="field-wrapper">';

            $field->display();
            echo '</div>';
        }

        echo '<input type="reset" value="Réinitialiser" />';
        echo '<input type="submit" value="Envoyer" />';
        echo '</form>';
    }

    // Permet de mettre à jour les valeurs des champs avec un tableau associatif associant à chaque nom de champs une nouvelle valeur
    // Renvoie un booléen qui est vrai quand il aucun des champs dit que la valeur qu'on lui donne n'est pas valide d'après sa méthode de vérification
    // L'intérêt est ici de pouvoir envoyer $_POST
    public function setValues($values) {
        $isValid = true;
        foreach ($this->fields as $field) {
            $name = $field->getName();
            $isValid = $isValid && $field->setValue($values[$name]);
        }
        return $isValid;
    }

}

?>