<?php

// Classe abstraite représentant n'importe quel champ du formulaire
abstract class AbstractField {

    // Liste des attributs interdits pour $additionalAttributes
    const FORBIDDEN_ATTRIBUTES = ['id', 'name', 'value', 'oninput'];

    private $customValidateFunction; // Fonction custom de validation PHP, analogue aux fonctions appelées dans la oninput="validate(this)"
    protected $id;
    protected $name;
    protected $label;
    protected $sublabel;
    protected $value;
    protected $oninput;
    private $additionalAttributes;
    private $forbiddenAdditionalAttributes;

    function __construct($id, $name) {
        $this->unsetCustomValidateFunction();
        $this->setId($id);
        $this->setName($name);
        $this->unsetLabel();
        $this->unsetSublabel();
        $this->unsetValue();
        $this->unsetOninput();
        $this->additionalAttributes = array();
        $this->forbiddenAdditionalAttributes = self::FORBIDDEN_ATTRIBUTES;
    }

    public function setCustomValidateFunction($customValidateFunction) {
        // On vérifie que le changement de fonction custom ne rend pas incohérente la valeur
        if (!$customValidateFunction($this->value))
            Util::throwInvalidArgumentException('customValidateFunction', $customValidateFunction, 'makes the value non-valid');
        $this->customValidateFunction = $customValidateFunction;
    }

    public function unsetCustomValidateFunction() {
        $this->customValidateFunction = null;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        if (!is_string($id)
            || preg_match(Util::SLUG_REGEX, $id) == 0)
            Util::throwInvalidArgumentException('id', $id, 'must be a string that starts with an alpha and contains only alphanumerics, underscores and hyphens');
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        if (!is_string($name)
            || preg_match(Util::SLUG_REGEX, $name) == 0)
            Util::throwInvalidArgumentException('name', $name, 'must be a string that starts with an alpha and contains only alphanumerics, underscores and hyphens');
        $this->name = $name;
    }

    public function getLabel() {
        return $this->label;
    }

    public function setLabel($label) {
        if (!is_string($label))
            Util::throwInvalidArgumentException('label', $label, 'must be a string');
        $this->label = $label;
    }

    public function unsetLabel() {
        $this->label = null;
    }

    public function getSublabel() {
        return $this->sublabel;
    }

    public function setSublabel($sublabel) {
        if (!is_string($sublabel))
            Util::throwInvalidArgumentException('sublabel', $sublabel, 'must be a string');
        $this->sublabel = $sublabel;
    }

    public function unsetSublabel() {
        $this->sublabel = null;
    }

    public function getValue() {
        return $this->value;
    }

    // Retourne si la valeur est valide ou pas
    public function setValue($value) {
        $this->value = $value;
        return $this->isValueValid($value);
    }

    public function unsetValue() {
        $this->value = null;
    }

    public function getOninput() {
        return $this->oninput;
    }

    public function setOninput($oninput) {
        $this->oninput = $oninput;
    }

    public function unsetOninput() {
        $this->oninput = null;
    }

    public function getAdditionalAttribute($key) {
        return $this->additionalAttributes[$key];
    }

    // Permet d'ajouter des attributs en plus, par exemple required
    public function setAdditionalAttribute($key, $value = null) {
        if (!is_string($key))
            Util::throwInvalidArgumentException('key', $key, 'must be a string');
        if (in_array($key, $this->forbiddenAdditionalAttributes))
            Util::throwInvalidArgumentException('key', $key, 'can\'t be an additional attributes, use the appripriate setter');
        $this->additionalAttributes[$key] = $value;
    }

    public function unsetAdditionalAttribute($key) {
        if (!is_string($key))
            Util::throwInvalidArgumentException('key', $key, 'must be a string');
        unset($this->additionalAttributes[$key]);
    }

    protected function addForbiddenAdditionalAttributes(...$forbiddenAdditionalAttributes) {
        $this->forbiddenAdditionalAttributes = array_merge($this->forbiddenAdditionalAttributes, $forbiddenAdditionalAttributes);
    }

    protected function removeForbiddenAdditionalAttributes(...$forbiddenAdditionalAttributes) {
        $this->forbiddenAdditionalAttributes = array_diff($this->forbiddenAdditionalAttributes, $forbiddenAdditionalAttributes);
    }

    // Affiche le label d'un champ
    public function displayLabel() {
        $required = (array_key_exists('required', $this->additionalAttributes)) ? '<small>*</small> ' : '';

        $sublabel = '';
        if (isset($this->sublabel))
            $sublabel = " <small>{$this->getSublabel()}</small>";

        if (isset($this->label))
            echo "<label for='{$this->getId()}'>{$required}{$this->getLabel()}{$sublabel}</label>";
    }

    // Affoche les attributs additionnels (ex : required) d'un champ
    protected function displayAdditionalAttributes() {
        foreach ($this->additionalAttributes as $key => $value) {
            if (isset($value))
                echo "$key='$value' ";
            else
                echo "$key ";
        }
    }

    // Affiche le champ, forcément hérité par les enfants
    abstract public function display();

    // Le nom laisse à désirer, mais c'est la fonction qui permet de savoir si le champ est valide sans prendre en compte la fonction custom
    // C'est-à-dire vérifier que s'il est requis, il n'est pas vide, ou encore si c'est un champ date que la valeur soit bien une date
    public function isValueFieldValid($value) {
        return !array_key_exists('required', $this->additionalAttributes) || !empty($this->getValue());
    }

    // Vérifie que la valeur soit valide, c'est-à-dire si elle est valide comme au dessus + si elle est avec la fonction custom, si cette dernière existe
    public function isValueValid($value) {
        return $this->isValueFieldValid($value)
            && (is_null($this->customValidateFunction) || ($this->customValidateFunction)($value));
    }

    public function __toString() {
        return get_class($this) . "#{$this->getId()}";
    }

}

?>