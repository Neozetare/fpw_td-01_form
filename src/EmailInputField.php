<?php

// Classe représentant un input email
class EmailInputField extends AbstractInputField {

    public function __construct($id, $name, $value = null) {
        parent::__construct('email', $id, $name);

        if (!is_null($value))
            $this->setValue($value);
    }

    // Un input email est valide s'il est valide comme champ (appel parent) et si c'est un email valide
    public function isValueFieldValid($value) {
        return parent::isValueFieldValid($value)
            && filter_var($value, FILTER_VALIDATE_EMAIL);
    }

}

?>