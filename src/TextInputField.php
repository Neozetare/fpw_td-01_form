<?php

// Classe représentant un text input
class TextInputField extends AbstractInputField {

    public function __construct($id, $name, $value = null) {
        parent::__construct('text', $id, $name);

        if (!is_null($value))
            $this->setValue($value);
    }

    // Un text input n'a pas besoin de validation spécifique

}

?>