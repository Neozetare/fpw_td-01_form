<?php

// Une classe représentant un textarea
class TextareaField extends AbstractField {

    public function __construct($id, $name, $value = null) {
        parent::__construct($id, $name);

        if (!is_null($value))
            $this->setValue($value);
    }

    public function display() {
        $this->displayLabel();

        echo '<textarea ';
        echo "id='{$this->getId()}' ";
        echo "name='{$this->getName()}' ";
        if ($this->getOninput())
            echo "oninput='{$this->getOninput()}' ";
        $this->displayAdditionalAttributes();
        echo ">{$this->getValue()}</textarea>";
    }

    // Un textarea n'a pas besoin de validation spécifique

}

?>