<?php

// Classe qui représente tous les <input>
abstract class AbstractInputField extends AbstractField {

    // Contrairement à un textarea, un input doit avoir un type
    private $type;

    public function __construct($type, $id, $name) {
        $this->addForbiddenAdditionalAttributes('type');

        $this->setType($type);
        parent::__construct($id, $name);
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        if (!is_string($type))
            Util::throwInvalidArgumentException('type', $type, 'must be a string');
        $this->type = $type;
    }

    // Implémentation de display() qui fait appel au displayLabel() et au displayAdditionalAttributes() du parent
    public function display() {
        $this->displayLabel();

        echo '<input ';
        echo "type='{$this->getType()}' ";
        echo "id='{$this->getId()}' ";
        echo "name='{$this->getName()}' ";
        echo "value='{$this->getValue()}' ";
        if ($this->getOninput())
            echo "oninput='{$this->getOninput()}' ";
        $this->displayAdditionalAttributes();
        echo "/>";
    }

}

?>