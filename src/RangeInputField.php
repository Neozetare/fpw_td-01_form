<?php

// Classe représentant un input range
class RangeInputField extends AbstractInputField {

    public function __construct($id, $name, $value = null) {
        parent::__construct('range', $id, $name);

        if (!is_null($value))
            $this->setValue($value);
    }

    // Un input range est valide s'il est valide comme champ (appel parent), s'il est numérique et s'il est bien entre ses bornes, si elles existes
    // En réalité, il faudrait aussi tester le step par exemple, mais il y a beaucoup de situations en fonction des attributs que je n'ai pas géré, notamment pour le range mais pour d'autres input aussi
    public function isValueFieldValid($value) {
        return parent::isValueFieldValid($value)
            && is_numeric($value)
            && (is_null($this->getAdditionalAttribute('min')) || $value >= $this->getAdditionalAttribute('min'))
            && (is_null($this->getAdditionalAttribute('max')) || $value <= $this->getAdditionalAttribute('max'));
    }

}

?>