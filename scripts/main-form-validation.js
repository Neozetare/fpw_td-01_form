document.addEventListener("DOMContentLoaded", () => {
    // Pour chaque champs ayant un oninput, on le lance pour vérifier initialement la valeur
    const fields = document.querySelectorAll("form[name='main-form'] [oninput]:not([oninput=''])");

    fields.forEach(field => {
        validate(field);
    });
});

// Fonction de vérification custom de l'élément
function validate(element) {
    // On récupère la fonction personnalisée
    let func;
    switch (element.name) {
        case "nom":
        case "prenoms":
            func = isValidName;
            break;

        case "codePostalResidence":
            func = isValidFrenchPostalCode;
            break;

        case "dateNaissance":
            func = isValidPastDate;
            break;

        default:
            func = _ => false;
    }

    // Si l'élément n'est pas requis et qu'il est vide OU si sa fonction de validation est vérifiée, on le déclare valide
    if (!element.required && element.value == ""
        || func(element.value)) {
        element.setCustomValidity("");
    } else {
        element.setCustomValidity(element.title);
    }
    // On affiche sa validité, comme indicateur
    element.reportValidity();
}

function isValidName(value) {
    return XRegExp("^(?:\\pL[\r\n\t\f\v ]*[.,'-]?[\r\n\t\f\v ]*)+$").test(value); // [\r\n\t\f\v ]* should be \s*, but it doesn't work?
}

// Requête vers une API pour savoir si un code postal est valide
// L'utilisation de XMLHttpRequest est déprécié, surtout en synchrone, mais c'était pour ne pas mettre d'ajax alors qu'on ne l'avait pas vu en cours
function isValidFrenchPostalCode(value) {
    const request = new XMLHttpRequest();
    request.open('GET', 'https://geo.api.gouv.fr/communes?codePostal=' + value, false);
    request.send()
    const response = JSON.parse(request.response);

    return  response.length > 0;
}

function isValidPastDate(value) {
    return new Date(value) <= new Date();
}