Par Thomas RAZAFINDRALAMBO
# FRW TP Formulaire
Fondamentaux de la Programmation Web\
LP Métiers de l'Informatique parcours Web et Mobile\
IUT Orléans, 2020
## Description
Le code est normalement assez commenté pour que vous vous y retrouveriez.
- `data` : fichiers de données (description du formulaire en JSON)
- `scripts` : scripts JavaScript (vérifications en JS)
- `src` : classes PHP
  - `Form.php` : formulaire
  - `FieldTests.php` : vérification PHP
  - `Util.php` : fonctions utiles
  - reste : champs
- `style` : feuilles CSS
## Instructions
`debug` ou `force` en arguments GET peuvent vous aider à vérifier mon PHP (voir `index.php`)\
Le projet est uploadé sur http://naeho.zawazawa.team/